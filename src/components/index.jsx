import Hero from "./Hero";
import Sale from "./Sale"
import FlexContent from "./FlexContent";
import Stories from "./Stories";
import Footer from "./Footer";
import Navbar from "./Navbar";
import Cart from "./Cart";


export
{ Hero,Sale,FlexContent,Stories,Footer,Navbar,Cart
} 