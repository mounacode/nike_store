import emptybag  from '../../assets/emptybag.png'
import { ArrowLeftIcon } from '@heroicons/react/24/outline'
const CardEmpty = ({onCarToggle}) => {
  return (
    <>
      <div className='flex items-center flex-col justify-center h-screen  px-11 text-center  gap-7'>
        <img src={emptybag} alt="emptybag/img" className='w-40 lg:w-36 sm:w-28 h-auto object-fill 
                              hover:scale-110 transition-all duration-300' />
        <button type='button' onClick={onCarToggle} className="button-theme bg-gradient-to-b  from-amber-500 to-orange-500
                        shadow-lg  shadow-orange-500 flex items-center justify-center text-slate-900 py-2 gap-3 px-2
                        text-sm font-semibold active:scale-110">
             <ArrowLeftIcon className='w-7 h-7 text-slate-900'/>
             <span className=' '>Back To Nike Store</span>
        </button>
      </div>
    </>
  )
}

export default CardEmpty
